package main

import (
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"sync"
	"time"

	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	"github.com/gin-gonic/gin"
	"github.com/skip2/go-qrcode"
)

var lock sync.Mutex

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.LoadHTMLFiles("control.html", "play.html")
	r.StaticFile("/bootstrap.min.css", "./bootstrap.min.css")
	r.Static("/media", "./media")

	r.GET("/", func(c *gin.Context) {
		// os.Remove("qr.png")
		c.Redirect(302, "/control")
	})

	r.GET("/control", func(c *gin.Context) {
		cmd := c.Query("cmd")
		if cmd != "" {
			lock.Lock()
			play(cmd)
			lock.Unlock()
			c.String(200, "OK!")
			return
		}
		c.HTML(200, "control.html", gin.H{
			"mp3": getMP3List(),
			"mp4": getMP4List(),
		})
	})

	r.GET("/play", func(c *gin.Context) {
		file := c.Query("file")
		c.HTML(200, "play.html", file)
	})

	qrcode.WriteFile("http://"+getIP()+":8848/", qrcode.Medium, 256, "qr.png")
	println("Please scan `qr.png` or visit:")
	println("http://" + getIP() + ":8848/")
	r.Run(":8848")
}

func getIP() string {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		println("net.Interfaces failed.")
		return ""
	}

	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()

			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						return ipnet.IP.String()
					}
				}
			}
		}
	}
	return "127.0.0.1"
}

func play(cmd string) {
	if cmd == "stop" {
		speaker.Close()
		return
	}

	f, err := os.Open("./media/" + cmd)
	if err != nil {
		log.Fatal(err)
		return
	}

	if cmd[len(cmd)-4:] == ".mp4" {
		exec.Command(`cmd`, `/c`, `start`, `http://`+getIP()+`:8848/play?file=`+cmd).Start()
		return
	}

	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
		return
	}
	speaker.Close()
	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	speaker.Play(streamer)
}

func getMP3List() []string {
	MP3List := []string{}

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
		return nil
	}

	info, err := ioutil.ReadDir(wd + "/media")
	if err != nil {
		log.Fatal(err)
		return nil
	}
	for i := range info {
		if info[i].Name()[len(info[i].Name())-4:] == ".mp3" {
			MP3List = append(MP3List, info[i].Name())
		}
	}

	return MP3List
}

func getMP4List() []string {
	MP4List := []string{}

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
		return nil
	}

	info, err := ioutil.ReadDir(wd + "/media")
	if err != nil {
		log.Fatal(err)
		return nil
	}
	for i := range info {
		if info[i].Name()[len(info[i].Name())-4:] == ".mp4" {
			MP4List = append(MP4List, info[i].Name())
		}
	}

	return MP4List
}
